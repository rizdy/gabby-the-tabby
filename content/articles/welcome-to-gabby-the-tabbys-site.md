---
title: Welcome To Gabby The Tabby's site!
date: Fri, 22 Apr 2016 14:55:34 +0000
draft: false
categories: ['Information']
---

My name is Samantha, and my husband Sidney and I have the most precious little kitty in the world! Her name is Gabitha LeeAnn, we call her "Gabby" for short!

{{< image "gabinsam.jpg" >}}

We first got Gabby when she was a little less than a year old, and we found her at the best little pet store in Lynchburg, Virginia - where we were vacationing for Thanksgiving. We fell in love with her right away and absolutely had to bring her back home with us!

{{< image "gabby-cage-1.jpg" >}}

{{< image "gabby-cage-2.jpg" >}}

{{< image "gabby-mommy-1.jpg" >}}    

So started a wonderful, sometimes trying, relationship with the world's cutest kitty.

Gabby is absolutely wonderful. She's now three years old and very, very playful. All of the time. Especially at five in the morning. She loves to go "hunting" at the windows, and even out on the porch, but she's a Fraidy Cat, so she always makes sure to have an exit.

Gabby weighs a perfect 9.5 pounds, exactly where she needs to be, and is her veterinarian's "Dental Health All Star". This is thanks to Hubby who measures out her food, and researches all of the ingredients and things before we switch her.

After we got her, in fact, Hubby spent God only knows how much on books and videos about cat behavior, biology, and health. Hours researching the animal online, as well. He wanted to make sure he knew everything there was to know about these precious animals before we got one!

We decided, with such a healthy, beautiful, kitty, how could we _not_ share her with the world!? So we decided to start an [instagram page](https://www.instagram.com/gabby.the.tabby/) & this blog.

We hope you enjoy seeing snapshots of our lives with this wonderful little creature!